//this file contains view related javascript functions related to what the user is seeing and interacting with


function renderTable() {//renders table from global 'visitors' object array //had container ID in    args
    let tableHTML = "<tr> <th>Name</th> <th>Address</th> <th>Phone</th> <th>Email</th> <th>Actions</th> </tr>";

    for(let i = 0; i < visitors.length; i++){
        tableHTML += "<tr>"
        tableHTML += "<td>" + visitors[i].fullName + "</td>"
        tableHTML += "<td>" + visitors[i].fullAddress + "</td>"
        tableHTML += "<td>" + visitors[i].phone + "</td>"
        tableHTML += "<td>" + visitors[i].email + "</td>"
        tableHTML += "<td>" + `<button onclick="editVisitor(${visitors[i].id})"><i class="fas fa-edit"></i></button>` +
                            `<button onclick="deleteVisitor(${visitors[i].id})"><i class="fas fa-trash-alt"></i></button>` + "</td>"
        tableHTML += "</tr>"
    }
    $('#visitorTable').html(tableHTML);
}

function showVisitors()  {//shows visitor container and hides all other site content containers 
    setAllCharacter("none", "", true);//----------------------------FIX
    $(`#logVisit`).css("display","flex");
    $(`#logVisit`).children().css("display","none");
    $(`#visitorTableContainer`).fadeIn();
}
function showList() {//shows visitor list and hides form 
    //hideAll();
    $(`#logVisit`).css("display","flex");
    $(`#visitorTableContainer`).css("display","flex");
}
function showForm() {//shows visitor form and hides list 
    $(`#visitorTableContainer`).fadeOut();
    hideAll();
    $(`#logVisit`).css("display","flex");
    $(`#formDiv`).fadeIn();
    $('#firstName').focus();
}

function clearForm() {//clears values on inputs so next time form is loaded they don't have old contents
    $("#visitForm").get(0).reset();//reset all input values
    $(".was-validated").removeClass("was-validated");//reset validity
}

function hideAll(){
    setAllCharacter("none", "", true);//----------------------------FIX
    $(`#logVisit`).children().css("display","none");
}