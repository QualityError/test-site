/*
Updated js to jquery

removed and combined smaller functions

added ability to toggle site theme back and forth though a link.
 - added css to end of header

changed how character cards were assigned their onclick functionality.

*/


function characterSelect(character){
    setAllCharacter("none")
    $(`#${character}`).css("display","flex");
    setAllCharacter("block", character);
    $("#main").setAttribute("flex-direction", "column");
}

function characterScreen(){
    setAllCharacter("none","characterPage", true)
    setAllCharacter("flex")
}

function setAllCharacter(str, elementClass = "characterCard", all = false){
    $characterCards = $("#main").children();
    $characterCards.each(function(index) {
        $card = $(this);
        if(all || $card.hasClass(elementClass)){
            $card.css("display",str);
        }
    });
}

//set character links
for(let i = 1; i < 10; i++){
    //console.log($(`#character${i}`));
    $(`#character${i}`).click(function(index){
        characterSelect(`character${i}`);
        console.log(`character${i}`)
    })
}

//Toggle Theme
$("#theme").click(function(){
    if($('#newStyle').length){
        $('#newStyle').remove();
    } else {//add
        $theme = $("<link>").attr("href","css/theme.css");
        $theme.attr("rel","stylesheet");
        $theme.attr("id","newStyle");
        $("head").append($theme);
        //$theme.insertAfter($("#styles"));
    }
});

//Nav Links
$("#navCharacter").click(characterScreen);
$("#navLink2").click(function(){
    setAllCharacter("none", "", true);
    $(`#creationTips`).css("display","flex");
});
$("#navLink3").click(function(){
    setAllCharacter("none", "", true);
    $(`#books`).css("display","flex");
});
$("#navLink4").click(function(){
    loadVisitors();
});

$("#visitForm").submit(function(){
    if($("#visitForm").get(0).checkValidity()){//dont add to table if invalid
        submitForm();
    }
});

$(document).ready(function () {
    initValidation("#formDiv");
});

function loadVisitors() {//visitors nav button
    showVisitors();
    renderTable();
    showList();
 }
 
 function submitForm() {//if valid form is submitted
    let newVisitor = new Visitor(0,//id of zero initially
        $('#firstName').val(),
        $('#lastName').val(),
        $('#address').val(),
        $('#city').val(),
        $('#state').val(),
        $('#zip').val(),
        $('#phone').val(),
        $('#email').val(),
        {
        google:$('#google').prop("checked"),
        friend:$('#friend').prop("checked"),
        newspaper:$('#newspaper').prop("checked")
        },
        $('#comment').val());
    modelAddVisitor(newVisitor);
    renderTable();
    showList();
 }
 
 function addVisitor() {//new visitor Button
    clearForm();
    showForm();
 }
 
 function deleteVisitor(id) {//delete button on table
    modelDeleteVisitor(id);
    renderTable();
    showList();
 }

 $("#formCancel").click(function(){
    choice = confirm("Cancel form?");
    if(choice){
       loadVisitors();
    }
 });