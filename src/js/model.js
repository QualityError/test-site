//this file represents your 'model' or data layer.
//The 'model' contains 'data' related functions as well as any static data you may create such as your list of visitors.

class Visitor{
    constructor(id, firstName, lastName, address, city, state, zip, phone, email, found, notes) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.city = city;
        this.state = state;
        this.zip = zip;
        this.phone = phone;
        this.email = email;
        this.found = found;
        this.notes = notes;
       }
    get fullName(){
        return this.firstName + " " + this.lastName;
    }
    get fullAddress(){
        return `${this.address}, ${this.city}, ${this.state} ${this.zip}`;
    }
}

var visitors = [ //default values
    new Visitor(1,"Adam","Jenkins","800 W University Pkwy","Orem","Utah","84097","801-444-5555","chickenArmada@frog.com",{google:true,friend:false,newspaper:true}, ""),
    new Visitor(2,"Bert","Huggly","297 E University Pkwy","Mapleton","Utah","84097","801-443-1586","refriedBeans@tutanota.com",{google:false,friend:true,newspaper:false}, ""),
    new Visitor(3,"Hugo","Gurll","205 E 700 S Unit A","Pleasant Grove","Utah","84026","385-323-7935","hugoboss@milk.com",{google:true,friend:true,newspaper:true}, "Posted From a Roxberry"),
    new Visitor(4,"Ivan","Inkling","1825 W Traverse Pkwy Ste A","Lehi","Utah","84043","801-225-7540","capital7@posteo.net",{google:true,friend:false,newspaper:true}, ""),
    new Visitor(5,"Adam","Jenkins","5406 W 11000 N","Highland","Utah", "84003","801-321-1234","adam@thesocks.com",{google:false,friend:false,newspaper:true}, ""),
    new Visitor(6,"Felix","Sibeni","7512 Campus View Dr Ste 110","West Jordan","Utah","84084","801-477-0180","felix@theBuckets.com",{google:true,friend:false,newspaper:false}, "When will there be more content?")
   ];
   
var largestID = 6;
function modelAddVisitor(visitor) {//adds new visitor to visitors
    largestID += 1;
    visitor.id = largestID;
    visitors.push(visitor);

}
function modelDeleteVisitor(id) {//removes visitor with given id
    var choice = confirm(`Are you sure you want to delete ${visitors[findVisitorIndex(id)].fullName}`);
    
    if (choice){
        visitors = visitors.filter(data => data.id != id);
    }
}
function findVisitor(id) {//return visitor with given id
    return visitors[findVisitorIndex(id)]
}
function findVisitorIndex(id) {
    for(let i = 0; i < visitors.length; i++){
        if(visitors[i].id == id){
            return i;
        }
    }
}

function editVisitor(id){
    alert(`Edit ID=${id}`);
}